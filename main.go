package main

import "fmt"

func main() {

	number := [7]int{9, 6, 2, 4, 7, 1, 11}

	fmt.Println("Max number is", maxValue(number))

	//ข้อ2
	test := [5]string{"red", "green", "blue", "yellow", "white"}

	// result := convertToMyModels(test)
	result := convertToMyModels(test)

	fmt.Println(result[0].index)
	fmt.Println(result[0].value)

	fmt.Println(result[1].index)
	fmt.Println(result[1].value)

	fmt.Println(result[2].index)
	fmt.Println(result[2].value)

	// fmt.Println(result)
}
