package main

import "fmt"

//ข้อ1
func maxValue(number [7]int) int {

	resultMax := 0

	for _, v := range number {

		if v > resultMax {

			resultMax = v
		}

	}
	fmt.Println("Max number is", resultMax)
	return resultMax
}

//ข้อ2
func convertToMyModels(color [5]string) []MyModel {

	var modelArr []MyModel

	for i, v := range color {
		var model MyModel
		model.index = i
		model.value = v

		modelArr = append(modelArr, model)
		// res1 = append(mymodel.value, v)
	}
	fmt.Println(modelArr)

	return modelArr
}
